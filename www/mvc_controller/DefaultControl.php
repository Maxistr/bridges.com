<?php
namespace mvc_controller;

use mvc_controller\Control;

/*
Класс наследник Control, содержит простой вызов mvc модели.
*/
class DefaultControl extends Control
{
	/*
	Метод вызывает класс Control с параметрами приема Get и нахождением ссылки в переменной ref
	*/
	static public function Start_default_control()
	{
		parent::Receiving_ref ("GET","ref");
		parent::Start_control();
	}
}
?>