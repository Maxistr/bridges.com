<?php
namespace mvc_controller;

use mvc_controller\GetParametr;

/*
Класс осуществляет управление и запуск необходимых файлов(контроллеров, его вида), а так же
осуществляет передачу параметров между ними.
*/
class Control
{
	static private $ref;// содержит имя файла

	/*
	Метод позволяет получить данные о имени файла с класса GetParametr
	*/
	static public function Receiving_ref ($method_access,$name_ref)
	{
		self::$ref = GetParametr::Get_ref($method_access,$name_ref);
	}

	/*
	Метод запускает контроллер по мени указанному в $ref
	*/
	static public function Start_controller ()
	{
		if (file_exists('controller/'.self::$ref.'.php'))
		{
			return require_once ('/controller/'.self::$ref.'.php');
		}
		else 
		{
			return $param = array('start_controller' => false);
		}
	}
	
	/*
	Метод запускает визуальное оформление по имени указанному в $ref
	$param - параметр который содержит все необходимые данные от контроллера, если
	таков существует
	*/
	static public function Start_view ($param)
	{
		if (file_exists('view/'.self::$ref.'.php'))
		{
			require_once ('/view/'.self::$ref.'.php');
			return true;
		}
		else if (file_exists('view/'.self::$ref.'.html'))
		{
			require_once ('/view/'.self::$ref.'.html');
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	Метод осуществляет связь контроллера с его видом и передачу параметров между ними с
	помощьсю переменной $param
	*/
	static public function Start_control ()
	{

		$param = self::Start_controller();

		$View_flag = self::Start_view($param);
		
		if ($param['start_controller'] === false && $View_flag == false)
		{
			throw new \LogicException('Указанный файл не найден');
		}
	}
	
}	

?>