<?php
namespace mvc_controller;

use mvc_controller\GETInformation;
use mvc_controller\POSTInformation;

/*
Класс отвечает за прием информации от пользователя при передачи методом GET или POST
*/
abstract class GetParametr
{
	static protected $info; 
	
	abstract static public function Get_information ($name_ref);
	
	/*
	Абстрактная функция приема данных в зависимоти от метода
	$method_access - указивается метод передечи (Get или Post)
	$name_ref - указивается имя переменной которую передали
	*/
	static private function Choose_method ($method_access,$name_ref)
	{
		if (is_string($method_access))
		{
			if (strtolower($method_access)=="get")
			{
				$var = new GETInformation();
				return $var->Get_information($name_ref);
			}
			else if (strtolower($method_access)=="post")
			{
				$var = new POSTInformation();
				return $var->Get_information($name_ref);
			}
			else
			{
				throw new \LogicException("Передан не верный параметр");
			}
		}
		else 
		{
			throw new \LogicException("Передан не верный параметр");
		}
	}
	
	/*
	Возвращает ссылку на файл, по которому будет производиться поиск файла в папках
	controller и view
	*/
	static public function Get_info ()
	{
		return self::$info;
	}
	
	static public function Get_ref ($method_access,$name_ref)
	{
		self::Choose_method($method_access,$name_ref);
		return self::Get_info();
	}
}
?>