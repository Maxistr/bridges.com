<?php
namespace mvc_controller;

use mvc_controller\GetParametr;

/*
Класс предназначен для получения информации методом POST
*/
class POSTInformation extends GetParametr
{
	
	/*
	Получение ссылки методом POST
	*/
	static public function Get_information ($name_ref)
	{
		if (is_string($name_ref))
		{
			if (isset($_POST[$name_ref]))
			{
				parent::$info = $name_ref;
			}
		}	
		else 
		{
			throw new \LogicException("Передан не верный параметр");
		}
	}
	
}

?>