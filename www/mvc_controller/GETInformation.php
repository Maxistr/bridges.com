<?php
namespace mvc_controller;

use mvc_controller\GetParametr;

/*
Класс предназначен для получения информации методом GET
*/
class GETInformation extends GetParametr
{
	
	/*
	Получение ссылки методом GET
	*/
	static public function Get_information ($name_ref)
	{
		if (is_string($name_ref))
		{	
			if (isset($_GET[$name_ref]))
			{
				parent::$info = $_GET[$name_ref];
			}
		}	
		else 
		{
			throw new \LogicException("Передан не верный параметр");
		}
	}
	
}

?>