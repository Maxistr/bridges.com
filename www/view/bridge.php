<html>
<head>
    <link rel="stylesheet" type="text/css" href="/view/bridge.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/view/assets/css/bootstrap-responsive.css" rel="stylesheet">
    <script src="/view/js/bootstrap.min.js"></script>
</head>
<body>
<? include "/header.html"; ?>
    <div class="container">

        <?php
        echo "<h1 class='name_bridge' align='center' class='bluetext'>".$param[0]['name']."</h1>";
        ?>

        <br>
        <p align="center"><img src="<?=$param[0]['photo']?>" class="image"></p>
        <br>

        <div class="text">
            <?=$param[0]['history'] ?>
        </div>
    </div>
</body>
</html>
