<?php
namespace db;
/*
класс отвечающий за работу с базой данных
*/
class ConnectToDb{
	static private $connection;
	static private $db;
	
	/*
	функция осуществляет подключение к заданной базе
	$host - хост к которому происходит подключение
	$user - логин пользователь базы данных, по которому получаем доступ
	$password - пароль пользователь базы данных, по которому получаем доступ
	$name_db - название базы к которой, непосредственно, подключаемся
	*/
	public static function connect($host,$user,$password,$name_db)
	{
		if (is_null(self::$connection)){
			if ( !is_string($host) || !is_string($user) || !is_string($password) || !is_string($name_db)){
				throw new \LogicException('заданны неверные параметры');
			}
			else{
			
				self::$connection = mysql_connect($host,$user,$password);
				self::$db = mysql_select_db($name_db);
				mysql_set_charset("utf8");
		
				if (!self::$connection || !self::$db){
					 throw new \LogicException(mysql_error());
				}
			}
		}
	}
	
	/*
	функция осуществляет указанный в строке запрос к базе данных
	$request - строка с запросом к базе данных
	*/
	public static function make_request($request)
	{
		if (!is_string($request))
		{
			throw new \LogicException('передан не верный аргумент');
		}
		else
		{
			$result = mysql_query($request);
			while ($res = mysql_fetch_array($result))
			{
			$arr[] = $res;
			}
			return $arr;
		}
	}
}


?>